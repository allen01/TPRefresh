//
//  ViewController.m
//  RefreshDemo
//
//  Created by 魏信洋 on 2017/7/10.
//  Copyright © 2017年 林晓斌. All rights reserved.
//

#import "ViewController.h"
#import "TPRefreshAgent.h"
#import "TPRefresh.h"

@interface ViewController ()<UITableViewDataSource,UITableViewDelegate>

/**
 *  tableView
 */
@property (nonatomic, strong) UITableView *tableView;

/**
 *  refresh
 */
@property (nonatomic, strong) TPRefreshAgent *refreshAgent;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"首页";
    [self.view addSubview:self.tableView];
//    self.automaticallyAdjustsScrollViewInsets = NO;
    [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"123"];
//    [self.tableView addObserver:self forKeyPath:@"contentOffset" options:NSKeyValueObservingOptionOld | NSKeyValueObservingOptionNew context:nil];
//    
    self.tableView.tp_header = [TPRefreshHeader refreshHeaderWithBlock:^{
        NSLog(@"12133213");
    }];
}

- (void)dealloc{
    [self.tableView removeObserver:self forKeyPath:@"contentOffset"];
}


#pragma mark - tableView dataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 3;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"123"];
    cell.textLabel.text = [NSString stringWithFormat:@"row-%zd",indexPath.row];
    return cell;
}

#pragma mark - tableView delegate
//- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
//    return 64;
//}


#pragma mark - getter

/*
 */
- (TPRefreshAgent *)refreshAgent
{
    if (!_refreshAgent) {
        _refreshAgent = [[TPRefreshAgent alloc] initWithRefreshTarget:_tableView action:nil];
    }
    return _refreshAgent;
}

/**
 tableView
 */
- (UITableView *)tableView
{
    if (!_tableView) {
        _tableView  = [[UITableView alloc] init];
        _tableView.frame = self.view.bounds;
        _tableView.dataSource = self;
        _tableView.delegate = self;
    }
    return _tableView;
}



@end
