//
//  TPRefreshAgent.m
//  RefreshDemo
//
//  Created by 魏信洋 on 2017/7/19.
//  Copyright © 2017年 林晓斌. All rights reserved.
//

#import "TPRefreshAgent.h"

@interface TPRefreshAgent()

/**
 *  refresh target
 */
@property (nonatomic, strong) UIScrollView *target;
/**
 *  action
 */
@property (nonatomic, assign) SEL action;

/**
 *  header
 */
@property (nonatomic, strong) UIView *tp_header;

/**
 *  status
 */
@property (nonatomic, strong) UILabel *statusLabel;

/**
 *  bool
 */
@property (nonatomic, assign) BOOL isStopDragging;

/**
 *
 */
@property (nonatomic, assign) BOOL isStartDragging;

/**
 *
 */

@end

@implementation TPRefreshAgent
#pragma mark - init methods
- (instancetype)initWithRefreshTarget:(UIScrollView *)targetView action:(SEL)action{
    self = [super init];
    if (self) {
        self.target = targetView;
        self.target.delegate = self;
        self.action = action;
        [self setHeaderAndFooter];
        //[self.target addObserver:self forKeyPath:@"contentOffset" options:NSKeyValueObservingOptionNew | NSKeyValueObservingOptionOld context:nil];
    }
    return self;
}

- (void)setHeaderAndFooter{
    [self.target addSubview:self.tp_header];
    [self.tp_header addSubview:self.statusLabel];
}

#pragma mark - control refresh

- (void)endRefresh{
    [UIView animateWithDuration:0.5 animations:^{
        self.target.frame = CGRectMake(0, 0, self.target.frame.size.width, self.target.frame.size.height);
    }];
}

- (void)begingRefresh{
    [UIView animateWithDuration:0.5 animations:^{
        self.target.contentOffset = CGPointMake(0, -128);
    }];
    
}

#pragma mark - uiscrollView delegate
//在滑动
- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    NSLog(@"scrollViewY==%lf",scrollView.contentOffset.y);
    if (scrollView.contentOffset.y < -64 * 2) {
        if (self.isStartDragging && !self.isStopDragging) {
            self.tp_header.backgroundColor = [UIColor purpleColor];
            self.statusLabel.text = @"松开可以刷新";
        }
        if (scrollView.contentOffset.y > -128 && scrollView.contentOffset.y < -64) {
            self.statusLabel.text = @"下拉可以刷新";
        }
    }
    
}
//停止滑动
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    scrollView.bounces = YES;
}

//开始拖拽
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    self.isStopDragging = NO;
    self.isStartDragging = YES;
}

//停止拖拽
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    self.isStopDragging = YES;
    self.isStartDragging = NO;
    scrollView.bounces = NO;
    if (scrollView.contentOffset.y < -128) {
        NSLog(@"请求数据");
        self.isRefreshing = YES;
        self.statusLabel.text = @"正在刷新";
        self.tp_header.backgroundColor = [UIColor yellowColor];
        [UIView animateWithDuration:0.2 animations:^{
            scrollView.frame = CGRectMake(0, 64, self.target.frame.size.width, self.target.frame.size.height);
        }];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            NSLog(@"等待2秒");
            [UIView animateWithDuration:0.5 animations:^{
                scrollView.frame = CGRectMake(0, 0, self.target.frame.size.width, self.target.frame.size.height);
            }];
            self.tp_header.backgroundColor = [UIColor redColor];
            self.statusLabel.text = @"已经是最新数据";
        });
    }
}

#pragma mark - getter
/**
 header
 */
- (UIView *)tp_header
{
    if (!_tp_header) {
        _tp_header = [[UIView alloc] init];
        _tp_header.backgroundColor = [UIColor redColor];
        _tp_header.frame = CGRectMake(0, -64, self.target.frame.size.width, 64);
    }
    return _tp_header;
}

/**
 status lable
 */
- (UILabel *)statusLabel
{
    if (!_statusLabel) {
        _statusLabel = [[UILabel alloc] init];
        _statusLabel.font = [UIFont systemFontOfSize:13];
        _statusLabel.textAlignment = NSTextAlignmentCenter;
        _statusLabel.frame = CGRectMake(0, 0, self.tp_header.frame.size.width, 64);
    }
    return _statusLabel;
}

@end
