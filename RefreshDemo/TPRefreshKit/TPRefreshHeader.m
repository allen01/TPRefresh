//
//  TPRefreshHeader.m
//  RefreshDemo
//
//  Created by 魏信洋 on 2017/7/28.
//  Copyright © 2017年 林晓斌. All rights reserved.
//

#import "TPRefreshHeader.h"
#import "TPRefreshManager.h"
#import "UIView+TPFrameExtension.h"
#import "UIScrollView+TPRefresh.h"

@interface TPRefreshHeader()

/**
 *  label
 */
@property (nonatomic, strong) UILabel *headerLabel;

@end

@implementation TPRefreshHeader
//当header加入到父控件的时候,就会调用这个方法
- (void)willMoveToSuperview:(UIView *)newSuperview{
    [super willMoveToSuperview:newSuperview];
    if (![newSuperview isKindOfClass:[UIScrollView class]]) return;
    //新创建移除之前的监听
    [self removeObserver];
    if (newSuperview) {
        self.scrollView = (UIScrollView *)newSuperview;
        self.scrollView.alwaysBounceVertical = YES;
        [self setupSubviews];
        [self addObserver];
    }
}
//
//- (instancetype)init
//{
//    self = [super init];
//    if (self) {
//       // [self setupSubviews];
//    }
//    return self;
//}

- (void)setupSubviews{
    self.backgroundColor = [UIColor grayColor];
    self.frame = CGRectMake(0, -64, self.scrollView.width, 64);
    [self addSubview:self.headerLabel];
}
#pragma mark - Static methods

+ (instancetype)refreshHeaderWithTarget:(id)target selector:(SEL)selector{
    if (!target) return [[self alloc] init];
    
    [self setTarget:target selector:selector];
    
    return [[self alloc] init];
    
}

+ (instancetype)refreshHeaderWithBlock:(void (^)())block{
    [self setBlock:block];
    return [[self alloc] init];
}

+ (TPRefreshManager *)setTarget:(id)target selector:(SEL)selector{
    TPRefreshManager<TPRefreshHeaderProtocol> *manager = [[TPRefreshManager alloc] init];
    [manager setTarget:target selector:selector];
    return manager;
}

+ (TPRefreshManager *)setBlock:(void(^)())block{
    TPRefreshManager<TPRefreshHeaderProtocol> *manager = [[TPRefreshManager alloc] init];
    [manager setHeaderBlock:block];
    return manager;
}

#pragma mark - private methods

- (void)setRefreshState:(TPRefreshState)state{
    switch (state) {
        case TPRefreshStateIdle:
            self.headerLabel.text = @"下拉可以刷新";
            break;
        case TPRefreshStateWillRefresh:
            self.headerLabel.text = @"松开刷新";
            break;
        case TPRefreshStateRefreshing:
            self.headerLabel.text = @"正在刷新";
            break;
        case TPRefreshStateEnd:
            self.headerLabel.text = @"刷新完成";
            break;
    }
}

- (void)addObserver{
    [self.scrollView addObserver:self forKeyPath:@"contentOffset" options:NSKeyValueObservingOptionOld | NSKeyValueObservingOptionNew context:nil];
}

- (void)removeObserver{
    [self.scrollView removeObserver:self forKeyPath:@"contentOffset"];
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSKeyValueChangeKey,id> *)change context:(void *)context{
    if ([keyPath isEqualToString:@"contentOffset"]) {
        NSLog(@"%lf",self.scrollView.tp_offsetY);
        NSLog(@"insetTop:%lf",self.scrollView.tp_insetTop);
        [self changeState:change];
    }
}

- (void)changeState:(NSDictionary *)change{
    NSLog(@"%zd",self.scrollView.isDragging);
    if (self.scrollView.tp_insetTop == 64 && self.scrollView.tp_offsetY >= -64) {
        [self setRefreshState:TPRefreshStateIdle];
    }
    if (self.scrollView.tp_offsetY  < -64 * 2 && self.scrollView.isDragging) {
        [self setRefreshState:TPRefreshStateWillRefresh];
    }
    if (self.scrollView.tp_offsetY  >= -64 && !self.scrollView.isDragging){
        [self setRefreshState:TPRefreshStateIdle];
    }
    if (self.scrollView.tp_offsetY  < -64*2 && !self.scrollView.isDragging) {
        [self setRefreshState:TPRefreshStateRefreshing];
        self.scrollView.tp_insetTop = 128;
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [UIView animateWithDuration:0.5 animations:^{
                    self.scrollView.tp_insetTop = 64;
                    [self setRefreshState:TPRefreshStateEnd];
            }];
        });
    }
}

#pragma mark - getter

/**
   label
 */
- (UILabel *)headerLabel
{
    if (!_headerLabel) {
        _headerLabel = [[UILabel alloc] init];
        _headerLabel.text = @"下拉可以刷新";
        _headerLabel.textColor = [UIColor orangeColor];
        _headerLabel.textAlignment = NSTextAlignmentCenter;
        _headerLabel.frame = self.bounds;
    }
    return _headerLabel;
}

@end
