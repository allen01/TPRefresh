//
//  TPRefreshManager.m
//  RefreshDemo
//
//  Created by 魏信洋 on 2017/7/28.
//  Copyright © 2017年 林晓斌. All rights reserved.
//

#import "TPRefreshManager.h"

@implementation TPRefreshManager

- (void)setHeaderBlock:(void (^)())block{
    if (!block) return;
    self.block = block;
}

- (void)setTarget:(id)target selector:(SEL)selector{
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-performSelector-leaks" //去除警告
        //[target performSelector:selector];
#pragma clang diagnostic pop
    self.target = target;
    self.selector = selector;
}



@end
