//
//  TPRefreshHeaderProtocol.h
//  RefreshDemo
//
//  Created by 魏信洋 on 2017/7/28.
//  Copyright © 2017年 林晓斌. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol TPRefreshHeaderProtocol <NSObject>

- (void)setHeaderBlock:(void(^)())block;
- (void)setTarget:(id)target selector:(SEL)selector;

@end
