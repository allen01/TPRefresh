//
//  TPRefreshAgent.h
//  RefreshDemo
//
//  Created by 唐鹏 on 2017/7/19.
//  Copyright © 2017年 唐鹏. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TPRefreshAgent : NSObject<UIScrollViewDelegate,UITableViewDelegate>

@property (nonatomic, assign) BOOL isRefreshing;

- (instancetype)initWithRefreshTarget:(UIScrollView *)targetView action:(SEL)action;

- (void)endRefresh;

- (void)begingRefresh;

@end
