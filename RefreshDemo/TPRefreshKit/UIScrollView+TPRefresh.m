//
//  UIScrollView+TPRefresh.m
//  RefreshDemo
//
//  Created by 魏信洋 on 2017/7/28.
//  Copyright © 2017年 林晓斌. All rights reserved.
//

#import "UIScrollView+TPRefresh.h"
#import <objc/message.h>
static char *TPRefreshHeaderKey = "TPRefreshHeader";

@implementation UIScrollView (TPRefresh)


- (UIView *)tp_header{
    return objc_getAssociatedObject(self, TPRefreshHeaderKey);
}

- (void)setTp_header:(UIView *)tp_header{
    [self addSubview:tp_header];
    objc_setAssociatedObject(self, TPRefreshHeaderKey, tp_header, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (void)setTp_offsetY:(CGFloat)tp_offsetY{
    CGPoint offset = self.contentOffset;
    offset.y = tp_offsetY;
    self.contentOffset = offset;
}

- (CGFloat)tp_offsetY{
    return self.contentOffset.y;
}

- (void)setTp_offsetX:(CGFloat)tp_offsetX{
    CGPoint offset = self.contentOffset;
    offset.x = tp_offsetX;
    self.contentOffset = offset;
}

- (CGFloat)tp_offsetX{
    return self.contentOffset.x;
}

- (void)setTp_insetTop:(CGFloat)tp_insetTop{
    UIEdgeInsets edge = self.contentInset;
    edge.top = tp_insetTop;
    self.contentInset = edge;
}

- (CGFloat)tp_insetTop{
    return self.contentInset.top;
}

@end
