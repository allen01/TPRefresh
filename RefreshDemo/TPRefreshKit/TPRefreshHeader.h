//
//  TPRefreshHeader.h
//  RefreshDemo
//
//  Created by 魏信洋 on 2017/7/28.
//  Copyright © 2017年 林晓斌. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, TPRefreshState) {
    TPRefreshStateIdle,//默认从0开始
    TPRefreshStateWillRefresh,
    TPRefreshStateRefreshing,
    TPRefreshStateEnd
};

@interface TPRefreshHeader : UIView
+ (instancetype)refreshHeaderWithTarget:(id)target selector:(SEL)selector;
+ (instancetype)refreshHeaderWithBlock:(void(^)())block;
/**
 *  superView-->ScrollView
 */
@property (nonatomic, strong) UIScrollView *scrollView;
@end
