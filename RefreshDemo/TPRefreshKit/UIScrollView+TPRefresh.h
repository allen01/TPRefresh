//
//  UIScrollView+TPRefresh.h
//  RefreshDemo
//
//  Created by 魏信洋 on 2017/7/28.
//  Copyright © 2017年 林晓斌. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIScrollView (TPRefresh)

/**
 *  header
 */
@property (nonatomic, strong) UIView *tp_header;

/**
 *  offsetY
 */
@property (nonatomic, assign) CGFloat tp_offsetY;
/**
 *  offsetX
 */
@property (nonatomic, assign) CGFloat tp_offsetX;

/**
 *  inset top
 */
@property (nonatomic, assign) CGFloat tp_insetTop;



@end
