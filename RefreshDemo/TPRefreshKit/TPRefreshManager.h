//
//  TPRefreshManager.h
//  RefreshDemo
//
//  Created by 魏信洋 on 2017/7/28.
//  Copyright © 2017年 林晓斌. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TPRefreshHeaderProtocol.h"
@interface TPRefreshManager : NSObject<TPRefreshHeaderProtocol>
/**
 *  target
 */
@property (nonatomic, strong) id target;
/**
 *  SEL
 */
@property (nonatomic, assign) SEL selector;
/**
 *  block
 */
@property (nonatomic, copy) void (^block)();

//- (void)changeState:(NSDictionary *)change withScrollView:(UIScrollView *)scrollView;

@end
