//
//  AppDelegate.h
//  RefreshDemo
//
//  Created by 魏信洋 on 2017/7/10.
//  Copyright © 2017年 林晓斌. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

